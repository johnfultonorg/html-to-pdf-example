# html-to-pdf-example

See https://www.npmjs.com/package/vue-html2pdf for documentation
THis example based on https://medium.com/@toakshay.official/vue-js-generate-pdf-from-html-f095cf72bff4

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
